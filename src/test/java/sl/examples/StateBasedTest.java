package sl.examples;

import com.solidleon.slGameEngine.Application;
import com.solidleon.slGameEngine.BasicGameState;
import com.solidleon.slGameEngine.Graphics;
import com.solidleon.slGameEngine.StateBasedGame;

public class StateBasedTest extends StateBasedGame {
    @Override
    public void initStatesList(Application app) {
        addState(new MainMenu());
        addState(new PlayState());
    }


    static class MainMenu extends BasicGameState {

        private StateBasedGame game;

        @Override
        public int getID() {
            return 0;
        }

        @Override
        public void init(Application app, StateBasedGame game) {

            this.game = game;
        }

        @Override
        public void enter(Application app, StateBasedGame game) {
            
        }

        @Override
        public void leave(Application app, StateBasedGame game) {

        }

        @Override
        public void update(Application app, StateBasedGame game, int delta) {

        }

        @Override
        public void render(Application app, StateBasedGame game, Graphics g) {
            g.drawString("PRESS KEY TO ENTER PLAY STATE", 100, 100);
        }

        @Override
        public void keyPressed(int key, char c) {
            game.enterState(1);
        }
    }
    static class PlayState extends BasicGameState {

        @Override
        public int getID() {
            return 1;
        }

        @Override
        public void init(Application app, StateBasedGame game) {

        }

        @Override
        public void enter(Application app, StateBasedGame game) {

        }

        @Override
        public void leave(Application app, StateBasedGame game) {

        }

        @Override
        public void update(Application app, StateBasedGame game, int delta) {

        }

        @Override
        public void render(Application app, StateBasedGame game, Graphics g) {
            g.drawString("PLAY STATE", 100, 100);
        }
    }

}
