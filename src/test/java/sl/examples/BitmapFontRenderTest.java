package sl.examples;

import com.solidleon.slGameEngine.*;
import com.solidleon.slGameEngine.FontBitmap;

import java.util.List;

public class BitmapFontRenderTest extends BasicGame {

    Font font;
    float angle;

    List<DisplayMode> vidModes;
    int vidModeCursor;
    private Application app;

    @Override
    public void init(Application app) {
        this.app = app;

        font = new FontBitmap("sl/ascii.png", 32, 16, 16);
        font = new AngelCodeFont("sl/defaultfont.fnt", "sl/defaultfont.png");

        vidModes = app.getDisplayModes();

        for (int i = 0; i < vidModes.size(); i++) {
            var mode = vidModes.get(i);
            if (mode.w == app.getWidth() && mode.h == app.getHeight())
                vidModeCursor = i;
        }

        app.getGraphics().setClearColor(Color.CORNFLOWERBLUE);
    }

    @Override
    public void update(Application app, float dt) {
        angle += 10 * dt;
        if (angle > 360)
            angle -= 360;

    }

    @Override
    public void render(Application app, Graphics g) {
        g.setFont(font);

        g.setColor(Color.GREEN);
        g.drawString(app.getFPS() + " FPS", 0, 0);
        g.drawString(app.getWidth() + "/" + app.getHeight(), 0, font.getLineHeight());
        g.drawString("Hello World", 100, 100);

        g.setColor(Color.SKYBLUE);
        g.drawOval(100, 200, 50, 50);
        g.drawOval(100, 250, 50, 20);
        g.fillOval(200, 200, 50, 50);
        g.fillOval(200, 250, 50, 20);

        g.fillOval(300, 250, 600, 600);

        g.setColor(Color.BLACK);
        g.drawOval(300, 250, 600, 600);

        g.setColor(Color.MAROON);
        g.fillArc(300 + 300 - 50, 250 + 300 - 50, 100, 100, 0, angle);

        for (int i = 0; i < vidModes.size(); i++) {
            var mode = vidModes.get(i);
            g.setColor(i == vidModeCursor ? Color.GREEN : Color.DARKGRAY);
            g.drawString(
                    mode.w + "/" + mode.h,
                    0,
                    font.getLineHeight() * (4 + i));
        }
    }

    @Override
    public void keyPressed(int key, char c) {

        if (Input.KEY_ESCAPE == key) {
            app.exit();
        }
        if (Input.KEY_UP == key)
            vidModeCursor--;
        if (Input.KEY_DOWN == key)
            vidModeCursor++;
        if (vidModeCursor < 0) vidModeCursor = vidModes.size() - 1;
        if (vidModeCursor > vidModes.size() - 1) vidModeCursor = 0;

        if (key == Input.KEY_ENTER
                || key == Input.KEY_SPACE) {
            var mode = vidModes.get(vidModeCursor);
            app.setDisplaySize(mode.w, mode.h);
        }
        if (key == Input.KEY_F) {
            app.setFullscreen(!app.isFullscreen());
        }
    }

    @Override
    public void keyReleased(int key, char c) {

    }
}
