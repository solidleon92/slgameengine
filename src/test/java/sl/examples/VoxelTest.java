package sl.examples;

import com.solidleon.slGameEngine.*;
import com.solidleon.slGameEngine.util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VoxelTest extends BasicGame {

    float x, y, z=70;
    float rx, ry, rz;
    List<Vertex> vertices = new ArrayList<>();

    float ldx, ldy, ldz;
    int octaveCount = 1;
    private boolean lightEnabled;
    private double pitch = 256;
    private float scaleBias = 2.0f;
    private Application app;

    @Override
    public void init(Application app) {
        this.app = app;

        Random random = new Random(0);
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 256; y++) {
                for (int z = 0; z < 16; z++) {
                    if (random.nextBoolean()) {
                        vertices.addAll(buildCube(x, y, z));
                    }
                }
            }
        }

        app.getGraphics().setClearColor(Color.SKYBLUE);
    }

    @Override
    public void update(Application app, float dt) {
        var input = app.getInput();

        if (input.isKeyDown(Input.KEY_W)) {
            z -= 100f * dt;
        }
        if (input.isKeyDown(Input.KEY_S)) {
            z += 100f * dt;
        }
        if (input.isKeyDown(Input.KEY_A)) {
            x -= 100f * dt;
        }
        if (input.isKeyDown(Input.KEY_D)) {
            x += 100f * dt;
        }
        if (input.isKeyDown(Input.KEY_Q)) {
            y += 100f * dt;
        }
        if (input.isKeyDown(Input.KEY_E)) {
            y -= 100f * dt;
        }
        if (input.isKeyDown(Input.KEY_X)) {
            rx += 10f * dt;
        }
        if (input.isKeyDown(Input.KEY_Y)) {
            ry += 10f * dt;
        }
        if (input.isKeyDown(Input.KEY_Z)) {
            rz += 10f * dt;
        }
        if (input.isKeyDown(Input.KEY_O)) {

            rx = 0;
            ry = 0;
            rz = 0;
            x = y = z = 0;
            x = 0;
            y = 0;
            z = 800;
        }

        if (input.isKeyDown(Input.KEY_I)) {

            rx = 0;
            ry = 0;
            rz = 0;
            x = y = z = 0;
            x = 1000;
            y = 50;
            z = 10;
        }

        if (input.isKeyDown(Input.KEY_9)) {

            rx = 0;
            ry = 0;
            rz = 0;
            x = y = z = 0;
            x = 0;
            y = 0;
            z = 10;
        }


        ldx += dt * 0.5;
        ldy += 0.25 * dt;

        regenFromNoise();
    }

    @Override
    public void render(Application app, Graphics g) {
        if (lightEnabled) {
            g.drawLight(Mathf.cosf(ldx), Mathf.sinf(ldy), Mathf.sinf(ldx), Color.WHITE, Color.WHITE);
        } else {
            g.disableLight();
        }

        g.enterPerspectiveMode(app.getWidth(), app.getHeight());
        g.pushTransform();
        g.setTransform(new Transform(-x, -y, -z, 1f, 1f, 1f, rx, ry, rz));
        var model = new Model(vertices);
        g.drawModel(model);
        g.popTransform();
        g.disableLight();

        g.enterOrthoMode(app.getWidth(), app.getHeight());
        g.setColor(Color.GREEN);
        g.drawString(app.getFPS() + " FPS | v: " + vertices.size() + " | p: " + x + "/" + y + "/" + z, 0, 0);
        g.drawString("Octaves: " + octaveCount + " | SB: " + scaleBias + " | P: " + pitch, 0, g.getFont().getLineHeight());


        String[] text = {
                "w/s : z-/+",
                "a/d : x-/+",
                "e/q : y-/+",
                "x   : rx+",
                "y   : ry+",
                "z   : rz+",
                "0   : reset  camera",
                "i   : camera preset 1",
                "9   : camera preset 2",
                "esc : exit",
                "L   : toggle light ",
                "1/2 : octave +/-",
                "3/4 : scale bias +/-",
                "5/6 : pitch incr/decr",
                "u   : regenerate terrain"

        };
        for (int i = 0; i < text.length; i++) {
            var s = text[i];
            g.drawString(s, 0, g.getFont().getLineHeight() * (2 + i));
        }


    }

    private void regenFromNoise() {

        vertices.clear();
        int xs = 30;
        int zs = 30;

        int ys = 10;
        for (int z = 0; z < zs; z++) {
            for (int x = 0; x < xs; x++) {
                float noise = 0.0f;
                float scale = 1f;
                float scaleAcc = 0.0f;
                double pitch = this.pitch;
                int octaves = octaveCount;
                for (int i = 0; i < octaves; i++) {
                    //double sample = SimplexNoise.noise((double) (x + this.x) / pitch, (double) (z + this.z) / pitch);
                    double sample = SimplexNoise.noise((double) (x) / pitch, (double) (z) / pitch);
                    sample = (sample + 1) / 2.0;
                    noise += sample * scale;
                    scaleAcc += scale;
                    scale /= scaleBias;
                    pitch /= 2.0;
                }
                float vNoise = noise / scaleAcc;
                int height = (int) (vNoise * ys) + 1;
                for (int y = 0; y < height; y++) {
                    if (x == 0 || x == xs - 1
                            || z == 0 || z == zs - 1 || y == height - 1)
                        vertices.addAll(buildCube(x, y, z));
                }
                //vertices.addAll(buildCube(x - xs / 2f, height, z + this.z - zs));
                //vertices.addAll(buildCube(x, height, z));
            }
        }
    }

    List<Vertex> buildCube(float x, float y, float z) {
        List<Vertex> vertices = new ArrayList<>();

        // FRONT FACE
        vertices.add(new Vertex(new Vec3f(0, 1, 1), new Vec3f(0, 0, 1), Color.WHITE, null));
        vertices.add(new Vertex(new Vec3f(0, 0, 1), new Vec3f(0, 0, 1), Color.WHITE, null));
        vertices.add(new Vertex(new Vec3f(1, 0, 1), new Vec3f(0, 0, 1), Color.WHITE, null));
        vertices.add(new Vertex(new Vec3f(1, 0, 1), new Vec3f(0, 0, 1), Color.WHITE, null));
        vertices.add(new Vertex(new Vec3f(1, 1, 1), new Vec3f(0, 0, 1), Color.WHITE, null));
        vertices.add(new Vertex(new Vec3f(0, 1, 1), new Vec3f(0, 0, 1), Color.WHITE, null));

        // LEFT FACE
        vertices.add(new Vertex(new Vec3f(0, 1, 0), new Vec3f(-1, 0, 0), Color.RED, null));
        vertices.add(new Vertex(new Vec3f(0, 0, 0), new Vec3f(-1, 0, 0), Color.RED, null));
        vertices.add(new Vertex(new Vec3f(0, 0, 1), new Vec3f(-1, 0, 0), Color.RED, null));
        vertices.add(new Vertex(new Vec3f(0, 0, 1), new Vec3f(-1, 0, 0), Color.RED, null));
        vertices.add(new Vertex(new Vec3f(0, 1, 1), new Vec3f(-1, 0, 0), Color.RED, null));
        vertices.add(new Vertex(new Vec3f(0, 1, 0), new Vec3f(-1, 0, 0), Color.RED, null));

        // RIGHT FACE
        vertices.add(new Vertex(new Vec3f(1, 1, 1), new Vec3f(1, 0, 0), Color.GREEN, null));
        vertices.add(new Vertex(new Vec3f(1, 0, 1), new Vec3f(1, 0, 0), Color.GREEN, null));
        vertices.add(new Vertex(new Vec3f(1, 0, 0), new Vec3f(1, 0, 0), Color.GREEN, null));
        vertices.add(new Vertex(new Vec3f(1, 0, 0), new Vec3f(1, 0, 0), Color.GREEN, null));
        vertices.add(new Vertex(new Vec3f(1, 1, 0), new Vec3f(1, 0, 0), Color.GREEN, null));
        vertices.add(new Vertex(new Vec3f(1, 1, 1), new Vec3f(1, 0, 0), Color.GREEN, null));

        // BACK FACE
        //vertices.add(new Vertex(new Vec3i(1, 1, 0), Color.BLUE, new Vec3i(0, 0, -1)));
        //vertices.add(new Vertex(new Vec3i(1, 0, 0), Color.BLUE, new Vec3i(0, 0, -1)));
        //vertices.add(new Vertex(new Vec3i(0, 0, 0), Color.BLUE, new Vec3i(0, 0, -1)));
        //vertices.add(new Vertex(new Vec3i(0, 0, 0), Color.BLUE, new Vec3i(0, 0, -1)));
        //vertices.add(new Vertex(new Vec3i(0, 1, 0), Color.BLUE, new Vec3i(0, 0, -1)));
        //vertices.add(new Vertex(new Vec3i(1, 1, 0), Color.BLUE, new Vec3i(0, 0, -1)));

        // TOP FACE
        vertices.add(new Vertex(new Vec3f(0, 1, 0), new Vec3f(0, 1, 0), Color.MAGENTA, null));
        vertices.add(new Vertex(new Vec3f(0, 1, 1), new Vec3f(0, 1, 0), Color.MAGENTA, null));
        vertices.add(new Vertex(new Vec3f(1, 1, 1), new Vec3f(0, 1, 0), Color.MAGENTA, null));
        vertices.add(new Vertex(new Vec3f(1, 1, 1), new Vec3f(0, 1, 0), Color.MAGENTA, null));
        vertices.add(new Vertex(new Vec3f(1, 1, 0), new Vec3f(0, 1, 0), Color.MAGENTA, null));
        vertices.add(new Vertex(new Vec3f(0, 1, 0), new Vec3f(0, 1, 0), Color.MAGENTA, null));

        // BOTTOM FACE
        //vertices.add(new Vertex(new Vec3i(0, 0, 0), Color.GOLD, new Vec3i(0, -1, 0)));
        //vertices.add(new Vertex(new Vec3i(1, 0, 0), Color.GOLD, new Vec3i(0, -1, 0)));
        //vertices.add(new Vertex(new Vec3i(1, 0, 1), Color.GOLD, new Vec3i(0, -1, 0)));
        //vertices.add(new Vertex(new Vec3i(1, 0, 1), Color.GOLD, new Vec3i(0, -1, 0)));
        //vertices.add(new Vertex(new Vec3i(0, 0, 1), Color.GOLD, new Vec3i(0, -1, 0)));
        //vertices.add(new Vertex(new Vec3i(0, 0, 0), Color.GOLD, new Vec3i(0, -1, 0)));

        for (Vertex vertex : vertices) {
            vertex.position.x += x;
            vertex.position.y += y;
            vertex.position.z += z;
        }

        return vertices;
    }

    public void setLightEnabled(boolean lightEnabled) {
        if (this.lightEnabled != lightEnabled) {
            this.lightEnabled = lightEnabled;
        }
    }

    @Override
    public void keyPressed(int key, char c) {
        if (Input.KEY_ESCAPE == key) {
            app.exit();
        }


        if (Input.KEY_L == key) {
            setLightEnabled(!lightEnabled);
        }


        boolean regen = false;
        if (Input.KEY_1 == key) {
            octaveCount++;
            regen = true;
        }
        if (Input.KEY_2 == key) {
            octaveCount--;
            regen = true;
        }
        if (Input.KEY_3 == key) {
            scaleBias += 0.2f;
            regen = true;
        }
        if (Input.KEY_4 == key) {
            scaleBias -= 0.2f;
            regen = true;
        }
        if (Input.KEY_5 == key) {
            pitch *= 2;
            regen = true;
        }
        if (Input.KEY_6 == key) {
            pitch /= 2;
            regen = true;
        }
        if (Input.KEY_U == key) {
            regen = true;
        }
        if (pitch < 2) {
            pitch = 2;
        }
        if (scaleBias < 0.1) {
            scaleBias = 0.1f;
        }
        if (regen)
            regenFromNoise();
    }

    @Override
    public void keyReleased(int key, char c) {

    }

}
