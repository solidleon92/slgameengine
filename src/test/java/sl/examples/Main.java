package sl.examples;

import com.solidleon.slGameEngine.Application;

public class Main {
    public static void main(String[] args)  {
        //var game = new Application(new StateBasedTest());
        var game = new Application(new VoxelTest());
        //var game = new Application(new BitmapFontRenderTest());
        boolean fullscreen = false;
        if (fullscreen) {
            var modes = game.getDisplay().getVideoModes(game.getDisplay().getPrimaryMonitor());
            var mode = modes.get(modes.size() - 1);
            game.getDisplay().setDisplaySize(mode.width(), mode.height());
            game.getDisplay().setFullscreen(true);
        } else {
            game.getDisplay().setDisplaySize(1280, 720);
            game.getDisplay().setFullscreen(false);
        }

        game.getDisplay().setTitle("Test");

        game.start();
    }
}
