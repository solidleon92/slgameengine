package com.solidleon.slGameEngine.font.effects;

import com.solidleon.slGameEngine.UnicodeFont;
import com.solidleon.slGameEngine.font.Glyph;

import java.awt.*;
import java.awt.image.BufferedImage;

public interface Effect {

    /**
     * Called to draw the effect.
     *
     * @param image The image to draw into
     * @param g The graphics context to use for applying the effect
     * @param unicodeFont The font being rendered
     * @param glyph The particular glyph being rendered
     */
    void draw (BufferedImage image, Graphics2D g, UnicodeFont unicodeFont, Glyph glyph);

}
