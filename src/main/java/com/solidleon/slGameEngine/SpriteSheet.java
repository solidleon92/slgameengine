package com.solidleon.slGameEngine;

public class SpriteSheet {

    private Image image;
    private final int tileWidth;
    private final int tileHeight;
    private final int spacing;

    public SpriteSheet(String ref, int tileWidth, int tileHeight, int spacing) {
        image = new Image(ref);
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.spacing = spacing;
    }

    public Image getSprite(int x, int y) {
        return image.getSubImage(x*(tileWidth+spacing), y*(tileWidth+spacing), tileWidth, tileHeight);
    }

}
