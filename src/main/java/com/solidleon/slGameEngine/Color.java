package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.gl.LegacyGL;

/**
 * RGBA Color wrapper
 */
public class Color {
    // COLORS - https://htmlcolorcodes.com/color-names/
    public static final Color BLACK = new Color(0x000000FF);
    public static final Color WHITE = new Color(0xFFFFFFFF);
    public static final Color SILVER = new Color(0xC0C0C0FF);
    public static final Color YELLOW = new Color(0xFFFF00FF);
    public static final Color MAGENTA = new Color(0xFF00FFFF);
    public static final Color FUCHSIA = new Color(0xFF00FFFF);
    public static final Color PURPLE = new Color(0x800080FF);
    public static final Color LIGHTGRAY = new Color(0xD3D3D3);
    public static final Color DARKGRAY = new Color(0xA9A9A9);
    public static final Color GRAY = new Color(0x808080);
    public static final Color ORANGE = new Color(0xFFA500FF);
    public static final Color BROWN = new Color(0xA52A2AFF);
    public static final Color OLIVE = new Color(0x808000FF);
    public static final Color TEAL = new Color(0x808080FF);
    public static final Color PINK = new Color(0xFFc0cbff);
    public static final Color LIGHTPINK = new Color(0xffb6c1ff);
    public static final Color GOLD = new Color(0xffd700ff);
    public static final Color PEACHPUFF = new Color(0xffdab9ff);
    public static final Color KHAKI = new Color(0xf0e68cff);
    public static final Color LAVENDER = new Color(0xe6e6faff);
    public static final Color PLUM = new Color(0xdda0ddff);
    public static final Color VIOLET = new Color(0xee82eeff);
    public static final Color INDIGO = new Color(0x4b0082ff);
    public static final Color SLATEBLUE = new Color(0x6a5acdff);
    public static final Color LIMEGREEN = new Color(0x32cd32ff);
    public static final Color LIGHTGREEN = new Color(0x90ee90ff);
    public static final Color PALEGREEN = new Color(0x98fb98ff);
    public static final Color SEAGREEN = new Color(0x2e8b57ff);
    public static final Color DARKGREEN = new Color(0x006400ff);
    public static final Color AQUAMARINE = new Color(0x7fffd4ff);
    public static final Color TURQUOISE = new Color(0x40e0d0ff);
    public static final Color CADETBLUE = new Color(0x5f9ea0ff);
    public static final Color STEELBLUE = new Color(0x4682b4ff);
    public static final Color LIGHTBLUE = new Color(0xadd8e6ff);
    public static final Color ROYALBLUE = new Color(0x4169e1ff);
    public static final Color TAN = new Color(0xd2b48cff);
    public static final Color SNOW = new Color(0xfffafaff);
    public static final Color BEIGE = new Color(0xf5f5dcff);
    // RED
    public static final Color RED = new Color(0xFF0000FF);
    public static final Color MAROON = new Color(0x800000FF);
    public static final Color INDIANRED = new Color(0xCD5c5cff);
    public static final Color LIGHTCORAL = new Color(0xF08080FF);
    public static final Color SALMON = new Color(0xFA8072FF);
    public static final Color CRIMSON = new Color(0xDC143CFF);
    public static final Color FIREBICK = new Color(0xB22222FF);
    public static final Color DARKRED = new Color(0x8B0000FF);
    // GREEN
    public static final Color GREEN = new Color(0x008000FF);
    public static final Color LIME = new Color(0x00FF00FF);
    // BLUE
    public static final Color BLUE = new Color(0x0000FFFF);
    public static final Color NAVY = new Color(0x000080FF);
    public static final Color SKYBLUE = new Color(0x87CEEBFF);
    public static final Color CORNFLOWERBLUE = new Color(0x6495ED);
    public static final Color AQUA = new Color(0x00FFFFFF);
    public static final Color CYAN = new Color(0x00FFFFFF);
    public static final Color ALICEBLUE = new Color(0xF0F8FFFF);


    public final float r, g, b, a;

    public Color(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public Color(float r, float g, float b) {
        this(r, g, b, 1.0f);
    }

    public Color(int rgba) {
        r = ((rgba >> 24) & 0xFF) / 255f;
        g = ((rgba >> 16) & 0xFF) / 255f;
        b = ((rgba >> 8) & 0xFF) / 255f;
        a = ((rgba) & 0xFF) / 255f;
    }

    public void bind() {
        LegacyGL.glColor4f(r, g, b, a);
    }
}
