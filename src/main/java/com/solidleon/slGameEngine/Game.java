package com.solidleon.slGameEngine;

public interface Game extends InputListener {

    void init(Application app);

    void update(Application app, float dt);

    void render(Application app, Graphics g);

}
