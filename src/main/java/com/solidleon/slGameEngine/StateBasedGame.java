package com.solidleon.slGameEngine;

import java.util.HashMap;
import java.util.Map;

public abstract class StateBasedGame implements Game {

    private Map<Integer, GameState> states = new HashMap<>();
    private GameState currentState;
    private Application app;

    public abstract void initStatesList(Application app);

    protected void addState(GameState state) {
        states.put(state.getID(), state);
        if (currentState == null)
            enterState(state.getID());
    }


    @Override
    public void init(Application app) {
        this.app = app;
        initStatesList(app);
        for (GameState value : states.values()) {
            value.init(app, this);
        }
    }

    @Override
    public void update(Application app, float dt) {
        currentState.update(app, this, (int) (dt * 1000.0));
    }

    @Override
    public void render(Application app, Graphics g) {
        currentState.render(app, this, g);
    }

    @Override
    public void keyPressed(int key, char c) {

        currentState.keyPressed(key, c);

    }

    @Override
    public void keyReleased(int key, char c) {
        currentState.keyReleased(key, c);
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        currentState.mousePressed(button, x, y);
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        currentState.mouseReleased(button, x, y);
    }

    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {
        currentState.mouseClicked(button, x, y, clickCount);
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        currentState.mouseMoved(oldx, oldy, newx, newy);
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        currentState.mouseDragged(oldx, oldy, newx, newy);
    }

    @Override
    public void mouseWheelMoved(int newValue) {
        currentState.mouseWheelMoved(newValue);
    }

    public void enterState(int newStateId) {
        enterState(newStateId, null, null);
    }

    public void enterState(int newStateId, StateTransition out, StateTransition in) {
        if (currentState != null) {
            currentState.leave(app, this);
        }
        currentState = states.get(newStateId);
        if (currentState != null ) {
            currentState.enter(app, this);
        }
    }
}
