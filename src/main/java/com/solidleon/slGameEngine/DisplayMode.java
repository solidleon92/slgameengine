package com.solidleon.slGameEngine;

public class DisplayMode {

    public final int w;
    public final int h;

    public DisplayMode(int w, int h) {
        this.w = w;
        this.h = h;
    }
}
