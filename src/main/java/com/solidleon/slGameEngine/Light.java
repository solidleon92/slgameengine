package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.util.Vec3f;

public class Light {

    public Vec3f position;
    public Color ambinent;
    public Color diffuse;

    public Light(Vec3f position, Color ambinent, Color diffuse) {
        this.position = position;
        this.ambinent = ambinent;
        this.diffuse = diffuse;
    }

}
