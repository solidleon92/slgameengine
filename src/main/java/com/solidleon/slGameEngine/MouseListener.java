package com.solidleon.slGameEngine;

public interface MouseListener {

    void mousePressed(int button, int x, int y);

    void mouseReleased(int button, int x, int y);

    void mouseClicked(int button, int x, int y, int clickCount);

    void mouseMoved(int oldx, int oldy, int newx, int newy);

    void mouseDragged(int oldx, int oldy, int newx, int newy);

    void mouseWheelMoved(int newValue);

}
