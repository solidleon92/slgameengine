package com.solidleon.slGameEngine.util;

/**
 * 3D Float Vector
 */
public class Vec3f {
    public float x, y, z;

    public Vec3f(float x, float y, float z) {

        this.x = x;
        this.y = y;
        this.z = z;
    }
}
