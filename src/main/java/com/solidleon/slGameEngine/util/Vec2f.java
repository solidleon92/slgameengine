package com.solidleon.slGameEngine.util;

/**
 * 2D Float Vector
 */
public class Vec2f {
    public float x, y;

    public Vec2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vec2f mul(Vec2i v) {
        x *= v.x;
        y *= v.y;
        return this;
    }

    public void mul(float s) {
        x *= s;
        y *= s;
    }

    public Vec2f sub(Vec2f v) {
        x -= v.x;
        y -= v.y;
        return this;
    }

    public float cross(Vec2f v) {
        return this.x * v.y - this.y * v.x;
    }
}
