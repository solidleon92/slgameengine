package com.solidleon.slGameEngine.util;

public class Transform {

    public float tx;
    public float ty;
    public float tz;
    public float sx;
    public float sy;
    public float sz;
    public float rx;
    public float ry;
    public float rz;

    public Transform(float tx, float ty, float tz, float sx, float sy, float sz, float rx, float ry, float rz) {
        this.tx = tx;
        this.ty = ty;
        this.tz = tz;
        this.sx = sx;
        this.sy = sy;
        this.sz = sz;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }

}
