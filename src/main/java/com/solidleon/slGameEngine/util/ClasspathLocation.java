package com.solidleon.slGameEngine.util;

import java.io.InputStream;
import java.net.URL;

public class ClasspathLocation implements ResourceLocation{
    @Override
    public InputStream getResourceAsStream(String ref) {
        var sanitizedRef = ref.replace('\\', '/');
        return ResourceLoader.class.getClassLoader().getResourceAsStream(sanitizedRef);
    }

    @Override
    public URL getResource(String ref) {
        var sanitizedRef = ref.replace('\\', '/');
        return ResourceLoader.class.getClassLoader().getResource(sanitizedRef);
    }
}
