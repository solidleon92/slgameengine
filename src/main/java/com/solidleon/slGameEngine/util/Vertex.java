package com.solidleon.slGameEngine.util;

import com.solidleon.slGameEngine.Color;

/**
 * Vertex data structure
 */
public class Vertex {
    public Vec3f position;
    public Vec3f normal;
    public Color color;
    public Vec2f texCoords;

    public Vertex(Vec3f position, Vec3f normal, Color color, Vec2f texCoords) {
        this.position = position;
        this.normal = normal;
        this.color = color;
        this.texCoords = texCoords;
    }
}
