package com.solidleon.slGameEngine.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ResourceLoader {

    private static List<ResourceLocation> locations = new ArrayList<>();

    static {

        addResourceLocation(new ClasspathLocation());
        addResourceLocation(new FileSystemLocation(new File(".")));
    }

    public static void addResourceLocation(ResourceLocation location) {
        locations.add(location);
    }

    public static void removeResourceLocation(ResourceLocation location) {
        locations.remove(location);
    }

    public static void removeAllResourceLocations() {
        locations.clear();
    }

    public static InputStream getResourceAsStream(String ref) {
        for (ResourceLocation location : locations) {
            var in = location.getResourceAsStream(ref);
            if (in != null) return new BufferedInputStream(in);
        }
        throw new RuntimeException("Resource not found: " + ref);
    }

    public static URL getResource(String ref) {
        URL url = null;
        for (ResourceLocation location : locations) {
            url = location.getResource(ref);
            if (url != null) break;
        }
        if (url == null)
            throw new RuntimeException("Resource not found: " + ref);
        return url;
    }

    public static boolean resourceExists(String ref) {
        for (ResourceLocation location : locations) {
            var url = location.getResource(ref);
            if (url != null) return true;
        }
        return false;

    }

}
