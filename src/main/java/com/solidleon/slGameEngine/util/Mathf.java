package com.solidleon.slGameEngine.util;

public class Mathf {


    public static final float PI = (float) Math.PI;
    public static final float DEG_TO_RAD = PI / 180f;
    public static final float RAD_TO_DEG = 180f / PI;


    public static float cosf(float a) {
        return (float) Math.cos(a);
    }

    public static float sinf(float a) {
        return (float) Math.sin(a);
    }

    public static float tanf(float a) {
        return (float) Math.tan(a);
    }

    public static float atan2f(float y, float x) {
        return (float) Math.atan2(y, x);
    }

    public static float powf(float a, float b) {
        return (float) Math.pow(a, b);
    }

    public static float abs(float a) {
        return Math.abs(a);
    }

    public static int abs(int a) {
        return Math.abs(a);
    }

    public static int min(int a, int b) {
        return Math.min(a, b);
    }

    public static float max(float a, float b) {
        return Math.max(a, b);
    }
    public static float max(int a, int b) {
        return Math.max(a, b);
    }

    public static float deg2rad(float deg) {
        return (float) Math.toRadians(deg);
    }

    public static float rad2deg(float rad) {
        return (float) Math.toDegrees(rad);
    }

    public static float rand() {
        return (float) Math.random();
    }

}
