package com.solidleon.slGameEngine.util;

/**
 * 2D Integer Vector
 */
public class Vec2i {

    public int x, y;

    public Vec2i(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vec2i mul(Vec2i v) {
        x *= v.x;
        y *= v.y;
        return this;
    }

    public Vec2i div(Vec2i v) {
        x = (int) ((float) x / v.x);
        y = (int) ((float) y / v.y);
        return this;
    }

    public Vec2i copy() {
        return new Vec2i(x, y);
    }
}
