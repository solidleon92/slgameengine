package com.solidleon.slGameEngine.util;

import com.solidleon.slGameEngine.gl.LegacyGL;
import com.solidleon.slGameEngine.gl.Texture;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static org.lwjgl.opengl.GL11C.*;
import static org.lwjgl.opengl.GL11C.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL12C.GL_CLAMP_TO_EDGE;

public class BufferedImageUtil {
    public static Texture getTexture(String toString, BufferedImage imgTemp) throws IOException {
        var texID = LegacyGL.glGenTextures();
        var width = imgTemp.getWidth();
        var height = imgTemp.getHeight();
        Texture t = new Texture(texID, width, height);

        var pixels = ((DataBufferByte)imgTemp.getRaster().getDataBuffer()).getData();
        var imageBuffer = ByteBuffer
                .allocateDirect(pixels.length)
                .order(ByteOrder.nativeOrder())
                .put(pixels, 0, pixels.length)
                .flip();
        var format = LegacyGL.GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, texID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, imageBuffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        return null;
    }
}
