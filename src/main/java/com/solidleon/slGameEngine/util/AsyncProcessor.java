package com.solidleon.slGameEngine.util;

import java.util.List;
import java.util.ArrayList;

public class AsyncProcessor<T> {

  private final List<T> queue = new ArrayList<>();


  public void enqueue(T o) {
    synchronized(queue) {
      queue.add(o);
      queue.notifyAll();
    }
  }


  public T next() {
    T o = null;
    synchronized(queue) {
      if (!queue.isEmpty()) {
        o = queue.remove(0);
      }
    }
    return o;
  }


}
