package com.solidleon.slGameEngine.util;

public class Rectangle {

    public float x, y, w, h;

    public Rectangle(float x, float y, float w, float h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }
}
