package com.solidleon.slGameEngine.gl;

import static com.solidleon.slGameEngine.gl.GL.*;

/**
 * Wrapper for a OpenGL texture.
 */
public class Texture {

    /**
     * OpenGL texture ID
     * @see GL#glCreateTextures(int)
     */
    private final int texture;

    /** Texture width in pixels */
    private final int width;

    /** Texture height in pixels */
    private final int height;

    /**
     *
     * @param texture id for the texture created by {@link GL#glCreateTextures(int)}
     * @param width texture width in pixels
     * @param height texture height in pixels
     */
    public Texture(int texture, int width, int height) {
        this.texture = texture;
        this.width = width;
        this.height = height;
    }


    /**
     *
     * @return texture width in pixels
     */
    public int getWidth() {
        return width;
    }

    /**
     *
     * @return texture height in pixels
     */
    public int getHeight() {
        return height;
    }


    /**
     * Binds the texture to the current OpenGL context as a {@link GL#GL_TEXTURE_2D}
     * and sets blending to {@link GL#GL_SRC_ALPHA}, {@link GL#GL_ONE_MINUS_SRC_ALPHA}
     */
    public void bind() {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, texture);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    /**
     * Unbinds the texture and disables:
     * <ul>
     *     <li>{@link GL#GL_TEXTURE_2D}</li>
     *     <li>{@link GL#GL_BLEND}</li>
     * </ul>
     */
    public void unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
    }
}
