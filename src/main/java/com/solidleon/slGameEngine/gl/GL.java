package com.solidleon.slGameEngine.gl;

import org.lwjgl.opengl.GL46C;

/**
 * Wrapper for OpenGL calls only contains modern OpenGL (4.6 Core Profile).
 */
public class GL extends GL46C {
}
