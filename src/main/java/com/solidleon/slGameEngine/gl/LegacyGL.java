package com.solidleon.slGameEngine.gl;

import org.lwjgl.opengl.GL12;

/**
 * Wrapper for OpenGL calls that gives access to Legacy OpenGL (1.2).
 */
public class LegacyGL extends GL12 {

}
