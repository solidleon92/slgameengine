package com.solidleon.slGameEngine.gl;

import com.solidleon.slGameEngine.util.ResourceLoader;
import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static com.solidleon.slGameEngine.gl.GL.*;

public class TextureLoader {

    /**
     * Loads the given pixels as OpenGL texture.
     *
     * @param width  texture width in pixels
     * @param height  texture height in pixels
     * @param format  source texture format ({@link GL#GL_RGBA}, {@link GL#GL_RGB}, ...)
     * @param pixels  pixel data in given format
     * @return OpenGL Texture
     */
    public static Texture loadTexture(int width, int height, int format, ByteBuffer pixels) {
        int texture = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, pixels);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        return new Texture(texture, width, height);
    }

    /**
     * Loads the given resource as OpenGL texture.
     * <p>
     *     Uses a source format of either {@link GL#GL_RGBA} or {@link GL#GL_RGB}
     *     depending on the number of channels {@link org.lwjgl.stb.STBImage#stbi_load_from_memory(ByteBuffer, int[], int[], int[], int)} returns.
     * </p>
     *
     * @param res  path to the resource to load
     * @return OpenGL texture
     */
    public static Texture loadTexture(String res)  {
        try {
            var stream = ResourceLoader.getResourceAsStream(res);
            var data = stream.readAllBytes();
            IntBuffer bX = BufferUtils.createIntBuffer(1);
            IntBuffer bY = BufferUtils.createIntBuffer(1);
            IntBuffer bChannels = BufferUtils.createIntBuffer(1);
            var pixels = org.lwjgl.stb.STBImage.stbi_load_from_memory(BufferUtils.createByteBuffer(data.length).put(data).flip(), bX, bY, bChannels, 4);
            int width = bX.get(0);
            int height = bY.get(0);
            int channels = bChannels.get(0);
            int format = GL_RGBA;
            if (channels == 3) format = GL_RGB;
            return loadTexture(width, height, format, pixels);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
