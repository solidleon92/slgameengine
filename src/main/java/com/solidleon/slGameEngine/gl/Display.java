package com.solidleon.slGameEngine.gl;

import com.solidleon.slGameEngine.Application;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Display {

    private static final Logger logger = Logger.getLogger(Display.class.getName());

    private long window;
    private int width = 300;
    private int height = 300;
    private boolean fullscreen;
    private String title = "SLGameEngine Version " + Application.SLGE_VERSION;
    private boolean created;
    private int refreshRate;
    private DisplayListener listener;

    public Display(DisplayListener listener) {
        this.listener = Objects.requireNonNull(listener, "A DisplayListener is required");
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();
        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");
    }

    public void setVsyncEnabled(boolean vsync) {
        logger.info("setVsyncEnabled=" + vsync);
        glfwSwapInterval(vsync ? 1 : 0);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setTitle(String title) {
        this.title = title;
        if (created) {
            logger.info("Update glfw window title=" + title);
            glfwSetWindowTitle(window, title);
        }
    }

    public void create() {
        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        logMonitors();

        // Create the window
        if (fullscreen) {
            window = glfwCreateWindow(width, height, title, glfwGetPrimaryMonitor(), NULL);
        } else {
            window = glfwCreateWindow(width, height, title, NULL, NULL);
        }
        if (window == NULL)
            throw new RuntimeException("Failed to create the GLFW window");


        // Get the thread stack and push a new frame
        try (MemoryStack stack = stackPush()) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            if (vidmode != null) {
                refreshRate = vidmode.refreshRate();
                // Center the window
                glfwSetWindowPos(
                        window,
                        (vidmode.width() - pWidth.get(0)) / 2,
                        (vidmode.height() - pHeight.get(0)) / 2
                );
            }
        } // the stack frame is popped automatically


        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        setVsyncEnabled(false);



        // Make the window visible
        glfwShowWindow(window);

        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        org.lwjgl.opengl.GL.createCapabilities();

        createCallbacks();

        created = true;
    }

    private void createCallbacks() {
        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if (action == GLFW_PRESS) {
                listener.keyPressed(this, key, scancode, mods);
            } else if (action == GLFW_RELEASE) {
                listener.keyReleased(this, key, scancode, mods);
            }
        });
        glfwSetWindowSizeCallback(window, (window1, width1, height1) -> {
            this.width = width1;
            this.height = height1;
            listener.windowSizeChanged(this, width1, height1);
        });
        glfwSetFramebufferSizeCallback(window, (window1, width1, height1) -> {
            listener.framebufferSizeChanged(this, width1, height1);
        });
        glfwSetJoystickCallback((jid, event) -> {
            if (GLFW_CONNECTED == event) {
                listener.joystickConnected(this, jid);
            } else if (GLFW_DISCONNECTED == event) {
                listener.joystickDisconnected(this, jid);
            }
        });
        glfwSetMonitorCallback((monitor, event) -> {
            if (GLFW_CONNECTED == event) {
                listener.monitorConnected(this, monitor);
            } else if (GLFW_DISCONNECTED == event) {
                listener.monitorDisconnected(this, monitor);
            }

        });
        glfwSetMouseButtonCallback(window, (window1, button, action, mods) -> {
            if (GLFW_PRESS == action) {
                listener.mouseButtonPressed(this, button, mods);
            } else if (GLFW_RELEASE == action) {
                listener.mouseButtonReleased(this, button, mods);
            }
        });
        glfwSetCursorPosCallback(window, (window1, xpos, ypos) -> {
            listener.mousePositionChanged((int)xpos, (int)ypos);
        });
        glfwSetScrollCallback(window, (window1, xoffset, yoffset) -> {
            listener.mouseWheelChanged(xoffset, yoffset);
        });
        glfwSetWindowCloseCallback(window, window1 -> {
            listener.windowCloseRequested(this);
        });
        glfwSetWindowFocusCallback(window, (window1, focused) -> {
            if (focused) {
                listener.windowGainedFocus(this);
            } else {
                listener.windowLostFocus(this);
            }
        });
        glfwSetWindowIconifyCallback(window, (window1, iconified) -> {
            if (iconified) {
                listener.windowIconified(this);
            } else {
                listener.windowDeiconified(this);
            }
        });
        glfwSetWindowMaximizeCallback(window, (window1, maximized) -> {
            if (maximized) {
                listener.windowMaximized(this);
            } else{
                listener.windowRestored(this);
            }
        });
        glfwSetWindowPosCallback(window, (window1, xpos, ypos) -> {
            listener.windowPositionChanged((int) xpos, (int) ypos);
        });
        glfwSetWindowRefreshCallback(window, window1 -> {
            listener.windowContentAreaRefreshRequested(this);
        });
        glfwSetWindowContentScaleCallback(window, (window1, xscale, yscale) -> {
            listener.windowContentScaleChanged(this, xscale, yscale);
        });
    }

    public long getPrimaryMonitor() {
        return glfwGetPrimaryMonitor();
    }

    public List<Long> getMonitors() {
        List<Long> list = new ArrayList<>();
        var monitors = glfwGetMonitors();
        for (int i = 0; i < monitors.limit(); i++) {
            var monitor = monitors.get(i);
            list.add(monitor);
        }
        return list;
    }

    public List<GLFWVidMode> getVideoModes(long monitor) {
        List<GLFWVidMode> result = new ArrayList<>();
        var modes = glfwGetVideoModes(monitor);
        for (int i1 = 0; i1 < modes.limit(); i1++) {
            result.add(modes.get(i1));
        }
        return result;
    }

    private void logMonitors() {
        for (Long monitor : getMonitors()) {
            logger.info(String.format("Monitor[%d] name=%s", monitor, glfwGetMonitorName(monitor)));
            for (GLFWVidMode mode : getVideoModes(monitor)) {
                logger.info(String.format("Monitor[%d] mode %d/%d/%d",
                        monitor,
                        mode.width(),
                        mode.height(),
                        mode.refreshRate()));
            }
        }
    }

    public void dispose() {
        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        var errorCallbackHandle = glfwSetErrorCallback(null);
        if (errorCallbackHandle != null) {
            errorCallbackHandle.free();
        }
    }

    public boolean isCloseRequested() {
        return glfwWindowShouldClose(window);
    }

    public void update() {
        glfwSwapBuffers(window); // swap the color buffers

        // Poll for window events. The key callback above will only be
        // invoked during this call.
        glfwPollEvents();
    }

    public long getWindow() {
        return window;
    }

    public boolean isFullscreen() {
        return fullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        if (this.fullscreen != fullscreen) {
            this.fullscreen = fullscreen;
            if (created) {
                logger.info("Update glfw window fullscreen=" + fullscreen);
                if (fullscreen) {
                    glfwSetWindowMonitor(window, glfwGetPrimaryMonitor(), 0, 0, width, height, refreshRate);
                } else {
                    glfwSetWindowMonitor(window, NULL, 0, 0, width, height, refreshRate);
                    var vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
                    var primaryWidth = vidmode.width();
                    var primaryHeight = vidmode.height();
                    glfwSetWindowPos(window, (primaryWidth - width) / 2, (primaryHeight - height) / 2);
                }
            }
        }
    }

    public void setDisplaySize(int width, int height) {
        this.width = width;
        this.height = height;
        if (created) {
            logger.info("Update glfw window width=" + width + ", height=" + height);
            glfwSetWindowSize(window, width, height);
            if (!fullscreen) {
                var vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
                var primaryWidth = vidmode.width();
                var primaryHeight = vidmode.height();
                glfwSetWindowPos(window, (primaryWidth - width) / 2, (primaryHeight - height) / 2);
            }
        }
    }


}
