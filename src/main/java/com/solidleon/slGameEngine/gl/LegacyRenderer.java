package com.solidleon.slGameEngine.gl;

import com.solidleon.slGameEngine.Application;
import com.solidleon.slGameEngine.Color;
import com.solidleon.slGameEngine.Font;
import com.solidleon.slGameEngine.util.Mathf;
import org.joml.Matrix4f;

import static com.solidleon.slGameEngine.gl.LegacyGL.*;
import static com.solidleon.slGameEngine.util.Mathf.*;

/**
 * A OpenGL renderer that uses {@link LegacyGL}.
 */
public class LegacyRenderer {

    /**
     * Draws a line between two points in a given color.
     *
     * @param x1    start point
     * @param y1    start point
     * @param x2    end point
     * @param y2    end point
     * @param color line color
     */
    public static void drawLine(float x1, float y1, float x2, float y2, Color color) {
        drawLine(x1, y1, 1, x2, y2, 1, color.r, color.g, color.b, color.a);
    }

    /**
     * Draws a filled quad.
     *
     * @param x      start point
     * @param y      start point
     * @param width  width in pixels
     * @param height height in pixels
     * @param color  fill color
     */
    public static void fillRect(float x, float y, float width, float height, Color color) {
        drawQuad(x, y, x + width, y + height, color.r, color.g, color.b, color.a);
    }

    /**
     * Draws a outlined quad.
     *
     * @param x      start point
     * @param y      start point
     * @param width  width in pixels
     * @param height height in pixels
     * @param color  outline color
     */
    public static void drawRect(float x, float y, float width, float height, Color color) {
        drawQuad(x, y, x + width, y + 1, color.r, color.g, color.b, color.a);
        drawQuad(x, y + height - 1, x + width, y + height, color.r, color.g, color.b, color.a);
        drawQuad(x, y, x + 1, y + height, color.r, color.g, color.b, color.a);
        drawQuad(x + width - 1, y, x + width, y + height, color.r, color.g, color.b, color.a);
    }

    /**
     * Draws a filled partial circle (arc).
     *
     * @param x      start point
     * @param y      start point
     * @param width  width in pixels
     * @param height height in pixels
     * @param start  start angle
     * @param end    end angle
     * @param color  fill color
     */
    public static void fillArc(float x, float y, float width, float height, float start, float end, Color color) {
        float xc = x + width / 2f;
        float yc = y + height / 2f;
        float rx = width / 2f;
        float ry = height / 2f;

        float arcStart = (float) Math.toRadians(start);
        float arcEnd = (float) Math.toRadians(end);

        int segments = (int) Math.max(8, (max(rx, ry) / 10f) * 8);
        float section = (2 * Mathf.PI) / segments;

        for (int i = 0; i < segments; i++) {
            boolean done = false;

            float a = arcStart + section * i;
            float a2 = arcStart + section * (i + 1);

            if (a2 > arcEnd) {
                a2 = arcEnd;
                done = true;
            }

            float dx = cosf(a);
            float dy = -sinf(a);
            float dx2 = cosf(a2);
            float dy2 = -sinf(a2);
            float x2 = xc + dx * rx;
            float y2 = yc + dy * ry;
            float x3 = xc + dx2 * rx;
            float y3 = yc + dy2 * ry;
            drawTriangle(xc, yc, 1, x2, y2, 1, x3, y3, 1, color.r, color.g, color.b, color.a, 0, 0, 0, 0, 0, 0);

            if (done)
                return;
        }
    }

    /**
     * Draws a outlined partial circle (arc).
     *
     * @param x      start point
     * @param y      start point
     * @param width  width in pixels
     * @param height height in pixels
     * @param start  start angle
     * @param end    end angle
     * @param color  outline color
     */
    public static void drawArc(float x, float y, float width, float height, float start, float end, Color color) {
        float xc = x + width / 2f;
        float yc = y + height / 2f;
        float rx = width / 2f;
        float ry = height / 2f;

        float arcStart = (float) Math.toRadians(start);
        float arcEnd = (float) Math.toRadians(end);

        int segments = (int) Math.max(8, (max(rx, ry) / 10f) * 8);
        float section = (2 * Mathf.PI) / segments;

        for (int i = 0; i < segments; i++) {
            boolean done = false;

            float a = arcStart + section * i;
            float a2 = arcStart + section * (i + 1);

            if (a2 > arcEnd) {
                a2 = arcEnd;
                done = true;
            }

            float dx = cosf(a);
            float dy = -sinf(a);
            float dx2 = cosf(a2);
            float dy2 = -sinf(a2);
            float x2 = xc + dx * rx;
            float y2 = yc + dy * ry;
            float x3 = xc + dx2 * rx;
            float y3 = yc + dy2 * ry;
            drawLine(x2, y2, 1, x3, y3, 1, color.r, color.g, color.b, color.a);
            if (done)
                return;
        }
    }

    /**
     * Draws a filled circle.
     *
     * @param x      start point
     * @param y      start point
     * @param width  width in pixels
     * @param height height in pixels
     * @param color  fill color
     */
    public static void fillOval(float x, float y, float width, float height, Color color) {
        float xc = x + width / 2f;
        float yc = y + height / 2f;
        float rx = width / 2f;
        float ry = height / 2f;

        int segments = (int) Math.max(8, (max(rx, ry) / 10f) * 8);
        float section = (2 * Mathf.PI) / segments;

        for (int i = 0; i < segments; i++) {
            float a = section * i;
            float a2 = section * (i + 1);
            float dx = cosf(a);
            float dy = sinf(a);
            float dx2 = cosf(a2);
            float dy2 = sinf(a2);
            float x2 = xc + dx * rx;
            float y2 = yc + dy * ry;
            float x3 = xc + dx2 * rx;
            float y3 = yc + dy2 * ry;
            drawTriangle(xc, yc, 1, x2, y2, 1, x3, y3, 1, color.r, color.g, color.b, color.a, 0, 0, 0, 0, 0, 0);
        }
    }

    /**
     * Draws a outlined circle.
     *
     * @param x      start point
     * @param y      start point
     * @param width  width in pixels
     * @param height height in pixels
     * @param color  outline color
     */
    public static void drawOval(float x, float y, float width, float height, Color color) {
        float xc = x + width / 2f;
        float yc = y + height / 2f;
        float rx = width / 2f;
        float ry = height / 2f;
        int segments = (int) Math.max(8, (max(rx, ry) / 10f) * 8);
        float section = (2 * Mathf.PI) / segments;
        for (int i = 0; i < segments; i++) {
            float a = section * i;
            float a2 = section * (i + 1);
            float dx = cosf(a);
            float dy = sinf(a);
            float dx2 = cosf(a2);
            float dy2 = sinf(a2);
            float x2 = xc + dx * rx;
            float y2 = yc + dy * ry;
            float x3 = xc + dx2 * rx;
            float y3 = yc + dy2 * ry;
            drawLine(x2, y2, 1, x3, y3, 1, color.r, color.g, color.b, color.a);
        }

    }


    /**
     * Draws a texture.
     *
     * @param texture OpenGL texture to draw
     * @param x       start point
     * @param y       start point
     * @param width   width in pixels
     * @param height  height in pixels
     * @param color   tint color
     */
    public static void draw(Texture texture, float x, float y, float width, float height, Color color) {
        texture.bind();
        float r = color.r;
        float g = color.g;
        float b = color.b;
        float a = color.a;
        float x1 = x;
        float y1 = y;
        float x2 = x;
        float y2 = y + height;
        float x3 = x + width;
        float y3 = y + height;
        float x4 = x + width;
        float y4 = y;

        float u1 = 0;
        float v1 = 0;
        float u2 = 1;
        float v2 = 1;
        drawQuad(x1, y1, x2, y2, x3, y3, x4, y4, u1, v1, u2, v2, r, g, b, a);
        texture.unbind();
    }


    /**
     * Draws a texture.
     * <p>
     * Draws a textured quad at the 4 vertex points using the supplied texture coordinates.
     * This allows for sub-texture drawing and arbritary sized quads.
     * </p>
     * <p>
     * In order to proper draw, make sure to pass the 4 vertex points in counter-clockwise order!
     * </p>
     *
     * @param texture OpenGL texture
     * @param x       1st vertex screen point
     * @param y       1st vertex screen point
     * @param x1      2nd vertex screen point
     * @param y1      2nd vertex screen point
     * @param x2      3rd vertex screen point
     * @param y2      3rd vertex screen point
     * @param x3      4th vertex screen point
     * @param y3      4th vertex screen point
     * @param tx      OpenGL Texture UV start point
     * @param ty      OpenGL Texture UV start point
     * @param tw      OpenGL Texture UV end point
     * @param th      OpenGL Texture UV end point
     */
    public static void draw(Texture texture, float x, float y, float x1, float y1, float x2, float y2, float x3, float y3, float tx, float ty, int tw, int th) {
        texture.bind();
        float u1 = (tx) / texture.getWidth();
        float v1 = (ty) / texture.getHeight();
        float u2 = (tx + tw) / texture.getWidth();
        float v2 = (ty + th) / texture.getHeight();
        drawQuad(x, y, x1, y1, x2, y2, x3, y3, u1, v1, u2, v2, 1f, 1f, 1f, 1f);
        texture.unbind();
    }

    /**
     * Draw a texture.
     * <p>
     * Draws the full texture in its original size at the start point.
     * </p>
     *
     * @param texture OpenGL Texture
     * @param x       start point
     * @param y       start point
     */
    public static void draw(Texture texture, float x, float y) {
        texture.bind();
        float r = 1f;
        float g = 1f;
        float b = 1f;
        float a = 1f;

        float x1 = x;
        float y1 = y;

        float x2 = x;
        float y2 = y + texture.getHeight();

        float x3 = x + texture.getWidth();
        float y3 = y + texture.getHeight();

        float x4 = x + texture.getWidth();
        float y4 = y;

        float u1 = 0;
        float v1 = 0;
        float u2 = 1;
        float v2 = 1;
        drawQuad(x1, y1, x2, y2, x3, y3, x4, y4, u1, v1, u2, v2, r, g, b, a);
        texture.unbind();
    }


    /**
     * Draws a primitive quad in 3D space with 4 vertices, color and texture coordinates.
     *
     * @param x1 1st vertex
     * @param y1 1st vertex
     * @param z1 1st vertex
     * @param x2 2nd vertex
     * @param y2 2nd vertex  1st vertex
     * @param z2 2nd vertex
     * @param x3 3rd vertex
     * @param y3 3rd vertex
     * @param z3 3rd vertex
     * @param x4 4th vertex
     * @param y4 4th vertex
     * @param z4 4th vertex
     * @param r  red color component
     * @param g  green color component
     * @param b  blue color component
     * @param a  alpha color component
     * @param u1 texture coordinates mapped to x1
     * @param v1 texture coordinates mapped to y1
     * @param u2 texture coordinates mapped to x4
     * @param v2 texture coordinates mapped to y4
     */
    public static void drawQuad(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4, float z4, float r, float g, float b, float a, float u1, float v1, float u2, float v2) {
        glBegin(GL_QUADS);
        glColor4f(r, g, b, a);
        glTexCoord2f(u1, v1);
        glVertex3f(x1, y1, z1);

        glTexCoord2f(u1, v2);
        glVertex3f(x2, y2, z2);

        glTexCoord2f(u2, v2);
        glVertex3f(x3, y3, z3);

        glTexCoord2f(u2, v1);
        glVertex3f(x4, y4, z4);
        glEnd();
        Application.renderedQuads++;
    }

    /**
     * Draws a primitive quad in 2D space with 4 vertices, color and texture coordinates.
     *
     * @param x1 1st vertex
     * @param y1 1st vertex
     * @param x2 2nd vertex
     * @param y2 2nd vertex  1st vertex
     * @param x3 3rd vertex
     * @param y3 3rd vertex
     * @param x4 4th vertex
     * @param y4 4th vertex
     * @param r  red color component
     * @param g  green color component
     * @param b  blue color component
     * @param a  alpha color component
     * @param u1 texture coordinates mapped to x1
     * @param v1 texture coordinates mapped to y1
     * @param u2 texture coordinates mapped to x4
     * @param v2 texture coordinates mapped to y4
     */
    public static void drawQuad(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float u1, float v1, float u2, float v2, float r, float g, float b, float a) {
        glBegin(GL_QUADS);
        glColor4f(r, g, b, a);
        glTexCoord2f(u1, v1);
        glVertex2f(x1, y1);

        glTexCoord2f(u1, v2);
        glVertex2f(x2, y2);

        glTexCoord2f(u2, v2);
        glVertex2f(x3, y3);

        glTexCoord2f(u2, v1);
        glVertex2f(x4, y4);
        glEnd();
        Application.renderedQuads++;
    }

    /**
     * Draws a primitive quad in 2D space
     *
     * @param x1 top left vertex
     * @param y1 top left vertex
     * @param x2 bottom right vertex
     * @param y2 bottom right vertex
     * @param r  red color component
     * @param g  green color component
     * @param b  blue color component
     * @param a  alpha color component
     */
    public static void drawQuad(float x1, float y1, float x2, float y2, float r, float g, float b, float a) {
        glBegin(GL_QUADS);
        glColor4f(r, g, b, a);
        glVertex2f(x1, y1);
        glVertex2f(x2, y1);
        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
        glEnd();
        Application.renderedQuads++;
    }

    /**
     * Draws a primitive textured quad in 2D space.
     *
     * @param x1 top left vertex
     * @param y1 top left vertex
     * @param x2 bottom right vertex
     * @param y2 bottom right vertex
     * @param r  red color component
     * @param g  green color component
     * @param b  blue color component
     * @param a  alpha color component
     * @param u1 texture coordinates mapped to x1
     * @param v1 texture coordinates mapped to y1
     * @param u2 texture coordinates mapped to x2
     * @param v2 texture coordinates mapped to y2
     */
    public static void drawQuad(float x1, float y1, float x2, float y2, float r, float g, float b, float a, float u1, float v1, float u2, float v2) {
        glBegin(GL_QUADS);
        glColor4f(r, g, b, a);
        glTexCoord2f(u1, v1);
        glVertex2f(x1, y1);

        glTexCoord2f(u2, v1);
        glVertex2f(x2, y1);

        glTexCoord2f(u2, v2);
        glVertex2f(x2, y2);

        glTexCoord2f(u1, v2);
        glVertex2f(x1, y2);
        glEnd();
        Application.renderedQuads++;
    }

    /**
     * Draws a primitive triangle in 3D space.
     *
     * @param x1
     * @param y1
     * @param z1
     * @param x2
     * @param y2
     * @param z2
     * @param x3
     * @param y3
     * @param z3
     * @param r
     * @param g
     * @param b
     * @param a
     * @param u1
     * @param v1
     * @param u2
     * @param v2
     * @param u3
     * @param v3
     */
    public static void drawTriangle(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float r, float g, float b, float a, float u1, float v1, float u2, float v2, float u3, float v3) {
        glBegin(GL_TRIANGLES);

        glColor4f(r, g, b, a);

        glTexCoord2f(u1, v1);
        glVertex3f(x1, y1, z1);

        glTexCoord2f(u2, v2);
        glVertex3f(x2, y2, z2);

        glTexCoord2f(u3, v3);
        glVertex3f(x3, y3, z3);

        glEnd();
    }

    /**
     * Drwas a lines in 3D space.
     *
     * @param x1
     * @param y1
     * @param z1
     * @param x2
     * @param y2
     * @param z2
     * @param r
     * @param g
     * @param b
     * @param a
     */
    public static void drawLine(float x1, float y1, float z1, float x2, float y2, float z2, float r, float g, float b, float a) {
        glBegin(GL_LINES);
        glColor4f(r, g, b, a);
        glVertex3f(x1, y1, z1);
        glVertex3f(x2, y2, z2);
        glEnd();
    }

    public static void begin() {
        Application.renderedQuads = 0;
    }


    /**
     * Draws a string in 2D space.
     *
     * @param font
     * @param s
     * @param x
     * @param y
     * @param color
     */
    public static void drawString(Font font, String s, float x, float y, Color color) {
        font.drawString(x, y, s, color);
    }


    /**
     * Sets the screen clear color.
     *
     * @param color
     * @see GL#glClearColor(float, float, float, float)
     * @see GL#glClear(int)
     */
    public static void setClearColor(Color color) {
        glClearColor(color.r, color.g, color.b, color.a);
    }

    /**
     * Clears the OpenGL buffers color and depth buffer.
     */
    public static void clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
    }

    /**
     * Sets the OpenGL state for orthogonal 2D rendering.
     *
     * @param viewportWidth
     * @param viewportHeight
     */
    public static void enterOrthoMode(int viewportWidth, int viewportHeight) {
        GL.glDisable(GL_DEPTH_TEST);
        GL.glDisable(GL_SMOOTH);
        GL.glDisable(GL_CULL_FACE);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMultMatrixf(new org.joml.Matrix4f().setOrtho2D(0, viewportWidth, viewportHeight, 0).get(new float[16]));

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

    }

    /**
     * Sets the OpenGL state for perspective 3D rendering.
     *
     * @param viewportWidth
     * @param viewportHeight
     */
    public static void enterPerspectiveMode(int viewportWidth, int viewportHeight) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_SMOOTH);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);


        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMultMatrixf(new Matrix4f().setPerspective(45.0f, (float) viewportWidth / viewportHeight, 0.1f, 10000.0f).get(new float[16]));
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glClearDepth(1.0f);
    }
}
