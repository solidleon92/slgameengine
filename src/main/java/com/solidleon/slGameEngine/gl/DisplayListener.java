package com.solidleon.slGameEngine.gl;

/**
 * Low-Level listener interface for {@link Display}.
 *
 * <p>
 *     This listener is used to hook into the low-level framework.
 * </p>
 */
public interface DisplayListener {
    void keyPressed(Display display, int key, int scancode, int mods);
    void keyReleased(Display display, int key, int scancode, int mods);
    void windowSizeChanged(Display display, int width, int height);
    void framebufferSizeChanged(Display display, int width, int height);
    void joystickConnected(Display display, int jid);
    void joystickDisconnected(Display display, int jid);

    void monitorDisconnected(Display display, long monitor);
    void monitorConnected(Display display, long monitor);

    void mouseButtonPressed(Display display, int button, int mods);
    void mouseButtonReleased(Display display, int button, int mods);

    void windowCloseRequested(Display display);

    void windowGainedFocus(Display display);
    void windowLostFocus(Display display);

    void windowIconified(Display display);
    void windowDeiconified(Display display);

    void windowMaximized(Display display);
    void windowRestored(Display display);

    void windowPositionChanged(int newx, int newy);

    void windowContentAreaRefreshRequested(Display display);

    void windowContentScaleChanged(Display display, float xscale, float yscale);

    void mousePositionChanged(int newx, int newy);

    void mouseWheelChanged(double xoffset, double yoffset);
}
