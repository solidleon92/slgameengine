
/**
 *
 * TODO:
 * <ul>
 * <li>shader support
 * <li>VBO/VAO support
 * <li>SceneGraph
 * <li>particle system
 * <li>Tiled loading
 * <li>XML support
 * <li>JSON support
 * <li>Networking: Client
 * <li>Networking: Server
 * <li>Networking: Lobby
 * <li>Networking: Client-side
 * prediction
 * <li>2D Lighting
 * <li>2D Physics
 * <li>GUI
 * <li>ECS
 * <li>Sound: WAV
 * <li>Sound: OGG
 * <li>Sound: Modulation / Positional
 * Audio
 * <li>Controller/Joystick support
 * <li>Localization
 * <li>Resource management
 * <li>Configuration
 * <li>Logging
 * <li>Graphics: Framebuffer support
 * <li>TTF Font support
 * <li>Quaternion
 * </ul>
 *
 */
package com.solidleon.slGameEngine;

