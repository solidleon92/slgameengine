package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.util.Vertex;

import java.util.List;

public class Model {
    public List<Vertex> vertices;

    public Model(List<Vertex> vertices) {

        this.vertices = vertices;
    }
}
