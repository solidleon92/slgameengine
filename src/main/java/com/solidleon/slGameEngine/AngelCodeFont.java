package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.gl.GL;
import com.solidleon.slGameEngine.gl.LegacyGL;
import com.solidleon.slGameEngine.gl.Texture;
import com.solidleon.slGameEngine.util.ResourceLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class AngelCodeFont implements Font{

    /** The highest character that AngelCodeFont will support. */
    private static final int MAX_CHAR = 255;

    private Image fontImage;
    private CharDef[] chars;
    private int lineHeight;

    public AngelCodeFont(String fntFile, String imgFile)  {
        fontImage = new Image(imgFile);
        parseFnt(ResourceLoader.getResourceAsStream(fntFile));
    }

    private void parseFnt(InputStream fntFile) {
        try {
            // now parse the font file
            BufferedReader in = new BufferedReader(new InputStreamReader(fntFile));
            String info = in.readLine();
            String common = in.readLine();
            String page = in.readLine();

            Map kerning = new HashMap(64);
            List charDefs = new ArrayList(MAX_CHAR);
            int maxChar = 0;
            boolean done = false;
            while (!done) {
                String line = in.readLine();
                if (line == null) {
                    done = true;
                } else {
                    if (line.startsWith("chars c")) {
                        // ignore
                    } else if (line.startsWith("char")) {
                        CharDef def = parseChar(line);
                        if (def != null) {
                            maxChar = Math.max(maxChar, def.id);
                            charDefs.add(def);
                        }
                    }
                    if (line.startsWith("kernings c")) {
                        // ignore
                    } else if (line.startsWith("kerning")) {
                        StringTokenizer tokens = new StringTokenizer(line, " =");
                        tokens.nextToken(); // kerning
                        tokens.nextToken(); // first
                        short first = Short.parseShort(tokens.nextToken()); // first value
                        tokens.nextToken(); // second
                        int second = Integer.parseInt(tokens.nextToken()); // second value
                        tokens.nextToken(); // offset
                        int offset = Integer.parseInt(tokens.nextToken()); // offset value
                        List values = (List)kerning.get(new Short(first));
                        if (values == null) {
                            values = new ArrayList();
                            kerning.put(new Short(first), values);
                        }
                        // Pack the character and kerning offset into a short.
                        values.add(new Short((short)((offset << 8) | second)));
                    }
                }
            }

            chars = new CharDef[maxChar + 1];
            for (Iterator iter = charDefs.iterator(); iter.hasNext();) {
                CharDef def = (CharDef)iter.next();
                chars[def.id] = def;
            }

            // Turn each list of kerning values into a short[] and set on the chardef.
            for (Iterator iter = kerning.entrySet().iterator(); iter.hasNext(); ) {
                Map.Entry entry = (Map.Entry)iter.next();
                short first = ((Short)entry.getKey()).shortValue();
                List valueList = (List)entry.getValue();
                short[] valueArray = new short[valueList.size()];
                int i = 0;
                for (Iterator valueIter = valueList.iterator(); valueIter.hasNext(); i++)
                    valueArray[i] = ((Short)valueIter.next()).shortValue();
                chars[first].kerning = valueArray;
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to parse font file: " + fntFile);
        }
    }

    private CharDef parseChar(String line)  {
        CharDef def = new CharDef();
        StringTokenizer tokens = new StringTokenizer(line, " =");

        tokens.nextToken(); // char
        tokens.nextToken(); // id
        def.id = Short.parseShort(tokens.nextToken()); // id value
        if (def.id < 0) {
            return null;
        }
        if (def.id > MAX_CHAR) {
            throw new RuntimeException("Invalid character '" + def.id
                    + "': AngelCodeFont does not support characters above " + MAX_CHAR);
        }

        tokens.nextToken(); // x
        def.x = Short.parseShort(tokens.nextToken()); // x value
        tokens.nextToken(); // y
        def.y = Short.parseShort(tokens.nextToken()); // y value
        tokens.nextToken(); // width
        def.width = Short.parseShort(tokens.nextToken()); // width value
        tokens.nextToken(); // height
        def.height = Short.parseShort(tokens.nextToken()); // height value
        tokens.nextToken(); // x offset
        def.xoffset = Short.parseShort(tokens.nextToken()); // xoffset value
        tokens.nextToken(); // y offset
        def.yoffset = Short.parseShort(tokens.nextToken()); // yoffset value
        tokens.nextToken(); // xadvance
        def.xadvance = Short.parseShort(tokens.nextToken()); // xadvance

        def.init();

        if (def.id != ' ') {
            lineHeight = Math.max(def.height + def.yoffset, lineHeight);
        }

        return def;
    }

    @Override
    public void drawString(float x, float y, String text) {
        drawString(x,y,text,Color.WHITE);
    }

    @Override
    public void drawString(float x, float y, String text, Color col) {
        drawString(x,y,text,col,0,text.length()-1);
    }

    public void drawString(float x, float y, String text, Color col,
                           int startIndex, int endIndex) {
        fontImage.bind();
        LegacyGL.glColor4f(col.r, col.g, col.b, col.a);
        LegacyGL.glTranslatef(x, y, 0);
        render(text, startIndex, endIndex);
        LegacyGL.glTranslatef(-x, -y, 0);
        LegacyGL.glBindTexture(LegacyGL.GL_TEXTURE_2D, 0);
    }
    private void render(String text, int start, int end) {
        LegacyGL.glBegin(LegacyGL.GL_QUADS);

        int x = 0, y = 0;
        CharDef lastCharDef = null;
        char[] data = text.toCharArray();
        for (int i = 0; i < data.length; i++) {
            int id = data[i];
            if (id == '\n') {
                x = 0;
                y += getLineHeight();
                continue;
            }
            if (id >= chars.length) {
                continue;
            }
            CharDef charDef = chars[id];
            if (charDef == null) {
                continue;
            }

            if (lastCharDef != null) x += lastCharDef.getKerning(id);
            lastCharDef = charDef;

            if ((i >= start) && (i <= end)) {
                charDef.draw(x, y);
            }

            x += charDef.xadvance;
        }
        LegacyGL.glEnd();
    }
    public int getHeight(String text) {
        int lines = 0;
        int maxHeight = 0;
        for (int i = 0; i < text.length(); i++) {
            int id = text.charAt(i);
            if (id == '\n') {
                lines++;
                maxHeight = 0;
                continue;
            }
            // ignore space, it doesn't contribute to height
            if (id == ' ') {
                continue;
            }
            CharDef charDef = chars[id];
            if (charDef == null) {
                continue;
            }

            maxHeight = Math.max(charDef.height + charDef.yoffset,
                    maxHeight);
        }

        maxHeight += lines * getLineHeight();

        return maxHeight;
    }
    public int getWidth(String text) {

        int maxWidth = 0;
        int width = 0;
        CharDef lastCharDef = null;
        for (int i = 0, n = text.length(); i < n; i++) {
            int id = text.charAt(i);
            if (id == '\n') {
                width = 0;
                continue;
            }
            if (id >= chars.length) {
                continue;
            }
            CharDef charDef = chars[id];
            if (charDef == null) {
                continue;
            }

            if (lastCharDef != null) width += lastCharDef.getKerning(id);
            lastCharDef = charDef;

            if (i < n - 1) {
                width += charDef.xadvance;
            } else {
                width += charDef.width;
            }
            maxWidth = Math.max(maxWidth, width);
        }


        return maxWidth;
    }
    public int getLineHeight() {
        return lineHeight;
    }
    private class CharDef {
        /** The id of the character */
        public short id;
        /** The x location on the sprite sheet */
        public short x;
        /** The y location on the sprite sheet */
        public short y;
        /** The width of the character image */
        public short width;
        /** The height of the character image */
        public short height;
        /** The amount the x position should be offset when drawing the image */
        public short xoffset;
        /** The amount the y position should be offset when drawing the image */
        public short yoffset;

        /** The amount to move the current position after drawing the character */
        public short xadvance;
        /** The image containing the character */
        public Image image;
        /** The display list index for this character */
        public short dlIndex;
        /** The kerning info for this character */
        public short[] kerning;

        /**
         * Initialise the image by cutting the right section from the map
         * produced by the AngelCode tool.
         */
        public void init() {
            image = fontImage.getSubImage(x, y, width, height);
        }

        /**
         * @see java.lang.Object#toString()
         */
        public String toString() {
            return "[CharDef id=" + id + " x=" + x + " y=" + y + "]";
        }

        /**
         * Get the kerning offset between this character and the specified character.
         * @param otherCodePoint The other code point
         * @return the kerning offset
         */
        public int getKerning (int otherCodePoint) {
            if (kerning == null) return 0;
            int low = 0;
            int high = kerning.length - 1;
            while (low <= high) {
                int midIndex = (low + high) >>> 1;
                int value = kerning[midIndex];
                int foundCodePoint = value & 0xff;
                if (foundCodePoint < otherCodePoint)
                    low = midIndex + 1;
                else if (foundCodePoint > otherCodePoint)
                    high = midIndex - 1;
                else
                    return value >> 8;
            }
            return 0;
        }


        public void draw(float x, float y) {
            image.drawEmbedded(x + xoffset, y + yoffset, width, height);
        }
    }
}
