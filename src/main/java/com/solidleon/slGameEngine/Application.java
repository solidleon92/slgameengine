package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.gl.Display;
import com.solidleon.slGameEngine.gl.DisplayListener;
import com.solidleon.slGameEngine.util.Transform;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWVidMode;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.solidleon.slGameEngine.gl.GL.*;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwGetTime;

/**
 * Entry point for games using the SLGameEngine.
 */
public final class Application {


    public static final String SLGE_VERSION = "1.3.0";
    private static final Logger logger = Logger.getLogger(Application.class.getName());
    public static int renderedQuads = 0;
    private final Display display;
    private int fps;
    private int fpsCounter;
    private float fpsTimer;
    private Input input;
    private boolean exitRequested;
    private Game game;
    private Graphics graphics;

    public Application(Game game) {
        this.game = game;
        display = new Display(new DisplayListener() {
            @Override
            public void keyPressed(Display display, int key, int scancode, int mods) {
                input.setKeyState(key, true);
                game.keyPressed(key, (char) scancode);
            }

            @Override
            public void keyReleased(Display display, int key, int scancode, int mods) {
                input.setKeyState(key, false);
                game.keyReleased(key, (char) scancode);
            }

            @Override
            public void windowSizeChanged(Display display, int width, int height) {
                glViewport(0, 0, width, height);
                System.out.println("WINDOW SIZE: " + width + "/" + height);

                System.out.println("");
            }

            @Override
            public void framebufferSizeChanged(Display display, int width, int height) {
                System.out.println("FRAMEBUFFER SIZE: " + width + "/" + height);
            }

            @Override
            public void joystickConnected(Display display, int jid) {
            }

            @Override
            public void joystickDisconnected(Display display, int jid) {
            }

            @Override
            public void monitorDisconnected(Display display, long monitor) {
            }

            @Override
            public void monitorConnected(Display display, long monitor) {
            }

            @Override
            public void mouseButtonPressed(Display display, int button, int mods) {
                input.setMouseButtonState(button, true);
                game.mousePressed(button, input.getMouseX(), input.getMouseY());
            }

            @Override
            public void mouseButtonReleased(Display display, int button, int mods) {
                input.setMouseButtonState(button, false);
                game.mouseReleased(button, input.getMouseX(), input.getMouseY());
            }

            @Override
            public void windowCloseRequested(Display display) {
            }

            @Override
            public void windowGainedFocus(Display display) {
            }

            @Override
            public void windowLostFocus(Display display) {
            }

            @Override
            public void windowIconified(Display display) {
            }

            @Override
            public void windowDeiconified(Display display) {
            }

            @Override
            public void windowMaximized(Display display) {
            }

            @Override
            public void windowRestored(Display display) {
            }

            @Override
            public void windowPositionChanged(int newx, int newy) {
            }

            @Override
            public void windowContentAreaRefreshRequested(Display display) {
            }

            @Override
            public void windowContentScaleChanged(Display display, float xscale, float yscale) {
            }

            @Override
            public void mousePositionChanged(int newx, int newy) {
                int oldx = input.getMouseX();
                int oldy = input.getMouseY();
                input.setMousePosition(newx, newy);
                game.mouseMoved(oldx, oldy, newx, newy);
                if (input.isAnyMouseDown()) {
                    game.mouseDragged(oldx, oldy, newx, newy);
                }
            }

            @Override
            public void mouseWheelChanged(double xoffset, double yoffset) {
                game.mouseWheelMoved((int)yoffset);
            }
        });
    }

    public void setVsyncEnabled(boolean vsync) {
        display.setVsyncEnabled(vsync);
    }

    public int getWidth() {
        return display.getWidth();
    }

    public int getHeight() {
        return display.getHeight();
    }

    public void setTitle(String title) {
        display.setTitle(title);
    }

    public boolean isFullscreen() {
        return display.isFullscreen();
    }

    public void setFullscreen(boolean fullscreen) {
        display.setFullscreen(fullscreen);
    }

    public void setDisplaySize(int width, int height) {
        display.setDisplaySize(width, height);
    }

    public void run() {
        display.create();
        input = new Input();
        loop();
        display.dispose();
    }


    private void loop() {

        // Set the clear color
        glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

        glDisable(GL_TEXTURE_2D);
        glDisable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_DST_ALPHA);

        createGraphics();

        game.init(this);

        glViewport(0, 0, display.getWidth(), display.getHeight());

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        double last = glfwGetTime();
        while (!display.isCloseRequested() && !exitRequested) {

            double[] xpos = new double[1];
            double[] ypos = new double[1];
            glfwGetCursorPos(display.getWindow(), xpos, ypos);
            input.setMousePosition((int)xpos[0], (int) ypos[0]);

            var now = glfwGetTime();
            var dt = (float) (now - last);
            last = now;

            game.update(this, dt);
            render();

            display.update();

            fpsCounter++;
            fpsTimer += dt;
            if (fpsTimer >= 1) {
                fpsTimer = 0;
                fps = fpsCounter;
                fpsCounter = 0;
            }
        }
    }

    private void render() {
        graphics.clear();
        graphics.enterOrthoMode(getWidth(), getHeight());
        game.render(this, graphics);
    }

    private void createGraphics() {
        graphics = new Graphics();
        graphics.setTransform(createBasic2DTransform());
        graphics.setDefaultFont(createDefaultFont());
        graphics.setDefaultShader(createDefaultShader());
        graphics.resetFont();
        graphics.resetShader();
    }

    private Transform createBasic2DTransform() {
        return new Transform(0, 0, 0, 1f, 1f, 1f, 0f, 0f, 0f);
    }

    private Shader createDefaultShader() {
        return null;
    }

    private Font createDefaultFont() {
        return new AngelCodeFont("sl/defaultfont.fnt", "sl/defaultfont.png");
    }


    public int getFPS() {
        return fps;
    }


    public void start() {
        logger.info("slGameEngine Version " + SLGE_VERSION);
        logger.info("LWJGL " + Version.getVersion() + "!");
        logger.info("Java Vendor: " + System.getProperty("java.vendor"));
        logger.info("Java Version: " + System.getProperty("java.version"));
        logger.info("OS Arch: " + System.getProperty("os.arch"));
        logger.info("OS Name: " + System.getProperty("os.name"));
        logger.info("OS Version: " + System.getProperty("os.version"));
        run();
    }

    public Display getDisplay() {
        return display;
    }

    public Input getInput() {
        return input;
    }

    public void exit() {
        exitRequested = true;
    }

    public List<DisplayMode> getDisplayModes() {
        List<DisplayMode> modes = new ArrayList<>();
        for (GLFWVidMode videoMode : display.getVideoModes(display.getPrimaryMonitor())) {
            modes.add(new DisplayMode(videoMode.width(), videoMode.height()));
        }
        return modes;
    }

    public Graphics getGraphics() {
        return graphics;
    }


}
