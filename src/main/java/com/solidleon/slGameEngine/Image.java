package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.gl.LegacyGL;
import com.solidleon.slGameEngine.gl.Texture;
import com.solidleon.slGameEngine.gl.TextureLoader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static org.lwjgl.opengl.GL11C.*;
import static org.lwjgl.opengl.GL12C.GL_CLAMP_TO_EDGE;

public class Image {

    public static final int FILTER_LINEAR = 1;
    public static final int FILTER_NEAREST = 2;

    private Texture texture;

    private float textureOffsetX, textureOffsetY;
    private float textureWidth, textureHeight;
    private int width, height;

    public Image(String res, boolean flipped, int filter) {
        this(res);
    }

    public Image(String res) {
        texture = TextureLoader.loadTexture(res);
        width = texture.getWidth();
        height = texture.getHeight();
        textureWidth = 1f;
        textureHeight = 1f;
        textureOffsetX = 0;
        textureOffsetY = 0;
    }

    public Image(int width, int height) {
        int texture = glGenTextures();
        var format = LegacyGL.GL_RGBA;
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE,
                ByteBuffer.allocateDirect(width * height)
                        .order(ByteOrder.nativeOrder())
                        .flip());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    }

    private Image() {
    }

    public Image getSubImage(int x, int y, int width, int height) {

        float newTextureOffsetX = ((x / (float) this.width) * textureWidth) + textureOffsetX;
        float newTextureOffsetY = ((y / (float) this.height) * textureHeight) + textureOffsetY;
        float newTextureWidth = ((width / (float) this.width) * textureWidth);
        float newTextureHeight = ((height / (float) this.height) * textureHeight);

        Image sub = new Image();
        sub.texture = this.texture;
        sub.textureOffsetX = newTextureOffsetX;
        sub.textureOffsetY = newTextureOffsetY;
        sub.textureWidth = newTextureWidth;
        sub.textureHeight = newTextureHeight;

        sub.width = width;
        sub.height = height;
        return sub;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void bind() {
        texture.bind();
    }

    public void drawEmbedded(float x, float y, short width, short height) {
        LegacyGL.glTexCoord2f(textureOffsetX, textureOffsetY);
        LegacyGL.glVertex3f(x, y, 0);
        LegacyGL.glTexCoord2f(textureOffsetX, textureOffsetY + textureHeight);
        LegacyGL.glVertex3f(x, y + height, 0);
        LegacyGL.glTexCoord2f(textureOffsetX + textureWidth, textureOffsetY
                + textureHeight);
        LegacyGL.glVertex3f(x + width, y + height, 0);
        LegacyGL.glTexCoord2f(textureOffsetX + textureWidth, textureOffsetY);
        LegacyGL.glVertex3f(x + width, y, 0);
    }

    public void draw(float x, float y, float w, float h) {}
    public void drawCentered(float x, float y) {}

    public void setRotation(float deg) {}

    public void destroy() {

    }
}
