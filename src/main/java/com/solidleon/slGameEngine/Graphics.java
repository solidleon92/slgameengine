package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.gl.GL;
import com.solidleon.slGameEngine.gl.LegacyGL;
import com.solidleon.slGameEngine.gl.LegacyRenderer;
import com.solidleon.slGameEngine.util.Mathf;
import com.solidleon.slGameEngine.util.Rectangle;
import com.solidleon.slGameEngine.util.Transform;
import com.solidleon.slGameEngine.util.Vertex;
import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public class Graphics {

    private Font defaultFont;
    private Font font;
    private Color clearColor = Color.BLACK;
    private Color color = Color.WHITE;
    private Rectangle clip = null;
    private List<Transform> transformStack = new ArrayList<>();
    private Transform transform;
    private Shader shader;
    private Shader defaultShader;

    public Shader getDefaultShader() {
        return defaultShader;
    }

    public void setDefaultShader(Shader defaultShader) {
        this.defaultShader = defaultShader;
    }

    public Font getDefaultFont() {
        return defaultFont;
    }

    public void setDefaultFont(Font defaultFont) {
        this.defaultFont = defaultFont;
    }

    public void setClearColor(Color c) {
        clearColor = c;
    }

    public void pushTransform() {
        transformStack.add(0, transform);
        setTransform(new Transform(0, 0, 0, 1f, 1f, 1f, 0f, 0f, 0f));
    }


    public void popTransform() {
        setTransform(transformStack.remove(0));
    }

    public void setTransform(Transform t) {
        transform = t;
        LegacyGL.glLoadIdentity();
        LegacyGL.glTranslatef(t.tx, t.ty,t.tz);
        LegacyGL.glRotatef(t.rx, 1f, 0f, 0f);
        LegacyGL.glRotatef(t.ry, 0f,1f, 0f);
        LegacyGL.glRotatef(t.rz, 0f, 0f, 1f);
        LegacyGL.glScalef(t.sx, t.sy,t.sz);
    }

    public void translate(float tx, float ty) {
        transform.tx += tx;
        transform.ty += ty;
        setTransform(transform);
    }

    public void scale(float sx, float sy) {
        transform.sx += sx;
        transform.sy += sy;
        setTransform(transform);
    }

    public void rotate(float rx, float ry) {
        transform.rx += rx;
        transform.ry += ry;
        setTransform(transform);
    }

    public void setClip(float x, float y, float w, float h) {
        clip = new Rectangle(x, y, w, h);
    }

    public void resetClip() {
        clip = null;
    }

    public void resetFont() {
        font = defaultFont;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public void resetShader() {
        setShader(defaultShader);
    }

    public Shader getShader() {
        return shader;
    }

    public void setShader(Shader shader) {
        if (this.shader != null) {
            this.shader.stopUse();
        }
        this.shader = shader;
        if (this.shader != null) {
            this.shader.use();
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color c) {
        color = c;
    }

    public void drawString(String s, float x, float y) {
        LegacyRenderer.drawString(font, s, x, y, color);
    }

    public void drawRect(float x, float y, float w, float h) {
        LegacyRenderer.drawRect(x,y,w,h,color);
    }

    public void fillRect(float x, float y, float w, float h) {
        LegacyRenderer.fillRect(x,y,w,h,color);
    }

    public void drawLine(float x1, float y1, float x2, float y2) {
        LegacyRenderer.drawLine(x1,y1,x2,y2,color);
    }

    public void drawOval(float x, float y, float w, float h, int segments) {
        LegacyRenderer.drawOval(x,y,w,h,color);
    }

    public void drawOval(float x, float y, float w, float h) {
        LegacyRenderer.drawOval(x,y,w,h,color);
    }

    public void fillOval(float x, float y, float w, float h, int segments) {
        LegacyRenderer.fillOval(x,y,w,h,color);
    }

    public void fillOval(float x, float y, float w, float h) {
        LegacyRenderer.fillOval(x,y,w,h,color);
    }

    public void drawArc(float x, float y, float w, float h, float start, float end, int segments) {
        LegacyRenderer.drawArc(x,y,w,h,start,end,color);
    }

    public void drawArc(float x, float y, float w, float h, float start, float end) {
        LegacyRenderer.drawArc(x,y,w,h,start,end,color);
    }

    public void fillArc(float x, float y, float w, float h, float start, float end, int segments) {
        LegacyRenderer.fillArc(x,y,w,h,start,end,color);
    }

    public void fillArc(float x, float y, float w, float h, float start, float end) {
        LegacyRenderer.fillArc(x,y,w,h,start,end,color);
    }

    public void drawModel(Model model) {

        LegacyGL.glBegin(LegacyGL.GL_TRIANGLES);
        for (Vertex vertex : model.vertices) {
            if (vertex.normal != null) {
                LegacyGL.glNormal3f(vertex.normal.x, vertex.normal.y, vertex.normal.z);
            }
            if (vertex.color != null) {
                LegacyGL.glColor4f(vertex.color.r, vertex.color.g, vertex.color.b, vertex.color.a);
            }
            if (vertex.texCoords != null) {
                LegacyGL.glTexCoord2f(vertex.texCoords.x, vertex.texCoords.y);
            }
            LegacyGL.glVertex3f(vertex.position.x, vertex.position.y, vertex.position.z);
        }
        LegacyGL.glEnd();
    }

    public void disableLight() {
        LegacyGL.glDisable(LegacyGL.GL_COLOR_MATERIAL);
        LegacyGL.glDisable(LegacyGL.GL_LIGHTING);
        LegacyGL.glDisable(LegacyGL.GL_LIGHT1);
    }

    public void drawLight(float x, float y, float z, Color ambient, Color diffuse) {
        LegacyGL.glEnable(LegacyGL.GL_COLOR_MATERIAL);
        LegacyGL.glEnable(LegacyGL.GL_LIGHTING);
        LegacyGL.glEnable(LegacyGL.GL_LIGHT1);
        LegacyGL.glColorMaterial(LegacyGL.GL_FRONT_AND_BACK, LegacyGL.GL_AMBIENT_AND_DIFFUSE);
        if (ambient != null)
            LegacyGL.glLightfv(LegacyGL.GL_LIGHT1, LegacyGL.GL_AMBIENT, new float[]{ambient.r, ambient.g, ambient.b, ambient.a});
        if (diffuse != null)
            LegacyGL.glLightfv(LegacyGL.GL_LIGHT1, LegacyGL.GL_DIFFUSE, new float[]{diffuse.r, diffuse.g, diffuse.b, diffuse.a});
        LegacyGL.glLightfv(LegacyGL.GL_LIGHT1, LegacyGL.GL_POSITION, new float[]{x, y, z, 0f});
    }

    public void clear() {
        LegacyRenderer.setClearColor(clearColor);
        LegacyRenderer.clear();
    }




    public void enterOrthoMode(int viewportWidth, int viewportHeight) {
        GL.glDisable(GL_DEPTH_TEST);
        GL.glDisable(GL_SMOOTH);
        GL.glDisable(GL_CULL_FACE);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMultMatrixf(new org.joml.Matrix4f().setOrtho2D(0, viewportWidth, viewportHeight, 0).get(new float[16]));

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

    }

    public void enterPerspectiveMode(int viewportWidth, int viewportHeight) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
        glEnable(GL_SMOOTH);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);


        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMultMatrixf(new Matrix4f().setPerspective(45.0f, (float) viewportWidth / viewportHeight, 0.1f, 10000.0f).get(new float[16]));
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glClearDepth(1.0f);
    }
}
