package com.solidleon.slGameEngine;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.glfw.GLFW.*;

public class Input {
    public static int KEY_ESCAPE = GLFW_KEY_ESCAPE;
    public static int KEY_A = GLFW_KEY_A;
    public static int KEY_B = GLFW_KEY_B;
    public static int KEY_C = GLFW_KEY_C;
    public static int KEY_D = GLFW_KEY_D;
    public static int KEY_E = GLFW_KEY_E;
    public static int KEY_F = GLFW_KEY_F;
    public static int KEY_G = GLFW_KEY_G;
    public static int KEY_H = GLFW_KEY_H;
    public static int KEY_I = GLFW_KEY_I;
    public static int KEY_J = GLFW_KEY_J;
    public static int KEY_K = GLFW_KEY_K;
    public static int KEY_L = GLFW_KEY_L;
    public static int KEY_M = GLFW_KEY_M;
    public static int KEY_N = GLFW_KEY_N;
    public static int KEY_O = GLFW_KEY_O;
    public static int KEY_P = GLFW_KEY_P;
    public static int KEY_Q = GLFW_KEY_Q;
    public static int KEY_R = GLFW_KEY_R;
    public static int KEY_S = GLFW_KEY_S;
    public static int KEY_T = GLFW_KEY_T;
    public static int KEY_U = GLFW_KEY_U;
    public static int KEY_V = GLFW_KEY_V;
    public static int KEY_W = GLFW_KEY_W;
    public static int KEY_X = GLFW_KEY_X;
    public static int KEY_Y = GLFW_KEY_Y;
    public static int KEY_Z = GLFW_KEY_Z;
    public static int KEY_1 = GLFW_KEY_1;
    public static int KEY_2 = GLFW_KEY_2;
    public static int KEY_3 = GLFW_KEY_3;
    public static int KEY_4 = GLFW_KEY_4;
    public static int KEY_5 = GLFW_KEY_5;
    public static int KEY_6 = GLFW_KEY_6;
    public static int KEY_7 = GLFW_KEY_7;
    public static int KEY_8 = GLFW_KEY_8;
    public static int KEY_9 = GLFW_KEY_9;
    public static int KEY_0 = GLFW_KEY_0;
    public static int KEY_NP1 = GLFW_KEY_KP_1;
    public static int KEY_NP2 = GLFW_KEY_KP_2;
    public static int KEY_NP3 = GLFW_KEY_KP_3;
    public static int KEY_NP4 = GLFW_KEY_KP_4;
    public static int KEY_NP5 = GLFW_KEY_KP_5;
    public static int KEY_NP6 = GLFW_KEY_KP_6;
    public static int KEY_NP7 = GLFW_KEY_KP_7;
    public static int KEY_NP8 = GLFW_KEY_KP_8;
    public static int KEY_NP9 = GLFW_KEY_KP_9;
    public static int KEY_NP0 = GLFW_KEY_KP_0;
    public static int KEY_LEFT = GLFW_KEY_LEFT;
    public static int KEY_RIGHT = GLFW_KEY_RIGHT;
    public static int KEY_UP = GLFW_KEY_UP;
    public static int KEY_DOWN = GLFW_KEY_DOWN;
    public static int KEY_SPACE = GLFW_KEY_SPACE;
    public static int KEY_TAB = GLFW_KEY_TAB;
    public static int KEY_ENTER = GLFW_KEY_ENTER;
    public int mouseX;
    public int mouseY;

    private Map<Integer, Boolean> mousePressed = new HashMap<>();
    private Map<Integer, Boolean> keyPressed = new HashMap<>();


    public boolean isMouseDown(int button) {
        return mousePressed.getOrDefault(button, false);
    }


    public boolean isKeyDown(int key) {
        return keyPressed.getOrDefault(key, false);
    }


    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }


    void setMousePosition(int x, int y) {
        mouseX = x;
        mouseY = y;
    }

    void setKeyState(int key, boolean pressed) {
        keyPressed.put(key, pressed);
    }

    void setMouseButtonState(int button, boolean pressed) {
        mousePressed.put(button, pressed);
    }


    public boolean isAnyMouseDown() {
        for (Boolean value : mousePressed.values()) {
            if (value)
                return true;
        }
        return false;
    }
}
