package com.solidleon.slGameEngine;

public interface KeyListener {

    void keyPressed(int key, char c);

    void keyReleased(int key, char c);

}
