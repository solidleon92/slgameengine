package com.solidleon.slGameEngine;

import com.solidleon.slGameEngine.util.ResourceLoader;
import org.lwjgl.BufferUtils;

import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;

/**
 * A simple Font implementation using Bitmaps.
 */
public class FontBitmap implements Font {

    private final int size;
    private final int texture;
    private final int texWidth;
    private final int texHeight;
    private final int charWidth;
    private final int charHeight;
    private final int widthInCharacters;
    private final int heightInCharacters;

    public FontBitmap(String res, int size, int charWidth, int charHeight) {
        try {
            this.size = size;
            this.charWidth = charWidth;
            this.charHeight = charHeight;
            var stream = ResourceLoader.getResourceAsStream(res);
            var data = stream.readAllBytes();
            IntBuffer bX = BufferUtils.createIntBuffer(1);
            IntBuffer bY = BufferUtils.createIntBuffer(1);
            IntBuffer bChannels = BufferUtils.createIntBuffer(1);
            var pixels = org.lwjgl.stb.STBImage.stbi_load_from_memory(BufferUtils.createByteBuffer(data.length).put(data).flip(), bX, bY, bChannels, 0);

            texWidth = bX.get(0);
            texHeight = bY.get(0);
            int channels = bChannels.get(0);
            // channels
            //1                     grey
            //2                     grey, alpha
            //3                     red, green, blue
            //4                     red, green, blue, alpha
            int format = -1;
            if (channels == 1) throw new RuntimeException("Unsupported Image: 1 channel");
            if (channels == 2) throw new RuntimeException("Unsupported Image: 2 channels");
            if (channels == 3) format = GL_RGB;
            if (channels == 4) format = GL_RGBA;


            texture = glGenTextures();
            glBindTexture(GL_TEXTURE_2D, texture);
            glTexImage2D(GL_TEXTURE_2D, 0, format, texWidth, texHeight, 0, format, GL_UNSIGNED_BYTE, pixels);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);


            widthInCharacters = texWidth / charWidth;
            heightInCharacters = texHeight / charHeight;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void drawString(float x, float y, String text, Color col) {
        drawString(x, y, text, col, 0, text.length() - 1);
    }

    @Override
    public void drawString(float x, float y, String text) {
        drawString(x, y, text, Color.WHITE);
    }

    @Override
    public void drawString(float x, float y, String text, Color col, int startIndex, int endIndex) {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, getTexture());
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        var ca = text.toCharArray();

        var widthInCharacters = getWidthInCharacters();
        var heightInCharacters = getHeightInCharacters();
        var charWidth = getCharWidth();
        var charHeight = getCharHeight();
        var texWidth = getTexWidth();
        var texHeight = getTexHeight();
        var size = getSize();

        for (int i = 0; i < ca.length; i++) {
            var c = ca[i];
            if (c < (widthInCharacters * heightInCharacters)) {
                int sx = c % widthInCharacters;
                int sy = c / widthInCharacters;

                float u1 = sx * charWidth / (float) texWidth;
                float u2 = (sx + 1) * charWidth / (float) texWidth;
                float v1 = (sy) * charHeight / (float) texHeight;
                float v2 = (sy + 1) * charHeight / (float) texHeight;

                float x1 = x + i * size;
                float y1 = y;
                float x2 = x1 + size;
                float y2 = y1 + size;

                drawQuad(x1, y1, x2, y2, col.r, col.g, col.b, col.a, u1, v1, u2, v2);
            }
        }

        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);
    }

    private void drawQuad(float x1, float y1, float x2, float y2, float r, float g, float b, float a, float u1, float v1, float u2, float v2) {
        glBegin(GL_QUADS);
        glColor4f(r, g, b, a);
        glTexCoord2f(u1, v1);
        glVertex2f(x1, y1);

        glTexCoord2f(u2, v1);
        glVertex2f(x2, y1);

        glTexCoord2f(u2, v2);
        glVertex2f(x2, y2);

        glTexCoord2f(u1, v2);
        glVertex2f(x1, y2);
        glEnd();
        Application.renderedQuads++;
    }

    @Override
    public int getHeight(String str) {
        return size;
    }

    public int getWidth(String text) {
        return text.length() * size;
    }

    public int getLineHeight() {
        return size;
    }

    public int getSize() {
        return size;
    }

    public int getTexture() {
        return texture;
    }

    public int getTexWidth() {
        return texWidth;
    }

    public int getTexHeight() {
        return texHeight;
    }

    public int getCharWidth() {
        return charWidth;
    }

    public int getCharHeight() {
        return charHeight;
    }

    public int getWidthInCharacters() {
        return widthInCharacters;
    }

    public int getHeightInCharacters() {
        return heightInCharacters;
    }
}
