package com.solidleon.slGameEngine;

public interface GameState extends InputListener{
    int getID();


    void init(Application app, StateBasedGame game);

    void enter(Application app, StateBasedGame game);

    void leave(Application app, StateBasedGame game);

    void update(Application app, StateBasedGame game, int delta);

    void render(Application app, StateBasedGame game, Graphics g);
}
