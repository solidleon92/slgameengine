package com.solidleon.slGameEngine;

public interface Font {

    int getWidth(String str);

    int getHeight(String str);

    int getLineHeight();

    void drawString(float x, float y, String text);

    void drawString(float x, float y, String text, Color col);

    void drawString(float x, float y, String text, Color col, int startIndex, int endIndex);
}
