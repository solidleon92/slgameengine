package com.solidleon.slGameEngine;

public class SGException extends RuntimeException{
    public SGException() {
    }

    public SGException(String message) {
        super(message);
    }

    public SGException(String message, Throwable cause) {
        super(message, cause);
    }

    public SGException(Throwable cause) {
        super(cause);
    }

    public SGException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
