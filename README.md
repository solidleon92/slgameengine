- [Website](https://solidleon92.gitlab.io/slgameengine/)
- [Documentation](https://solidleon92.gitlab.io/slgameengine/latest/Documentation)
- [JavaDoc](https://solidleon92.gitlab.io/slgameengine/latest/JavaDoc)


Java Game Library containing everything
needed to start with game development.

- LWJGL 3 based
- Direct access to everything LWJGL and
  its dependencies provide
- Extra functionality that can be used
  as wanted

